#
# Created by pronind.com
#


a = [14, 1, 3, 4, 2, 6, 8, 2, 10, 0]
assert_a = [0, 1, 2, 2, 3, 4, 6, 8, 10, 14]

count = 0


def bubble_sort(arr):
    global count
    arr_len = len(arr)
    for i in range(arr_len):
        for k in range(arr_len-1, i, -1):
            count += 1
            if arr[k] <= arr[k-1]:
                arr[k], arr[k-1] = arr[k-1], arr[k]
    return arr


print("length ok", len(bubble_sort(a)) == len(assert_a))
print("order ok", bubble_sort(a) == assert_a)
print(count)
