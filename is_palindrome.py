#
# Created by pronind.com
#

a = 1234567890987654321
b = "asdfgfdsa"
c = "karandadnarak"
d = "not that string"
e = "12344321"
f = "zaq1221qaz"
g = '`'
h = '12'
i = '121'
j = ''


def is_palindrome(palindrome):
    palindrome = str(palindrome)
    length = len(palindrome)
    if length >= 1:
        if palindrome[:length // 2] == palindrome[length // 2 + 1:][::-1]:
            return True
        elif palindrome[:length // 2] == palindrome[length // 2:][::-1]:
            return True
        else:
            return False
    else:
        return True


assert is_palindrome(a) == True
assert is_palindrome(b) == True
assert is_palindrome(c) == True
assert is_palindrome(d) == False
assert is_palindrome(e) == True
assert is_palindrome(f) == True
assert is_palindrome(g) == True
assert is_palindrome(h) == False
assert is_palindrome(i) == True
assert is_palindrome(j) == True
