#
# Created by pronind.com
#

matrix_a = [
    [2, 2, 2, 2, 2, 1],
    [2, 0, 0, 0, 0, 1],
    [2, 0, 3, 3, 0, 1],
    [2, 0, 3, 3, 0, 1],
    [2, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1]
]

matrix_a_check = [
    [1, 2, 2, 2, 2, 2],
    [1, 0, 0, 0, 0, 2],
    [1, 0, 3, 3, 0, 2],
    [1, 0, 3, 3, 0, 2],
    [1, 0, 0, 0, 0, 2],
    [1, 1, 1, 1, 1, 1]
]

matrix_b = [
    [1, 0, 1],
    [0, 1, 0],
    [1, 0, 1],
    [0, 1, 0],
    [1, 0, 1],
    [0, 1, 0],
    [1, 0, 1],
    [0, 1, 0],
    [1, 0, 1],
    [0, 1, 0],
    [1, 0, 1],
    [0, 1, 0],
    [1, 0, 1]
]

matrix_b_check = [
    [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
    [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
    [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1]
]


matrix_c = [
    [1, 0],
    [0, 1]
]

matrix_c_check = [
    [0, 1],
    [1, 0]
]

matrix_d = [
    [1, 0, 0],
    [0, 1, 0],
    [0, 0, 1]
]

matrix_d_check = [
    [0, 0, 1],
    [0, 1, 0],
    [1, 0, 0]
]

matrix_e = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]

matrix_e_check = [
    [7, 4, 1],
    [8, 5, 2],
    [9, 6, 3]
]


def rotate_matrix_90(matrix):
    n = len(matrix[0])
    m = len(matrix)
    temp_matrix = []
    for i in range(n):
        a = []
        for k in range(m-1, -1, -1):
            a.append(matrix[k][i])
        temp_matrix.append(a)
    return temp_matrix


def rotate_matrix_90_dirty_hack(matrix):
    return list(zip(*matrix[::-1]))


assert matrix_a_check == rotate_matrix_90(matrix_a)
assert matrix_b_check == rotate_matrix_90(matrix_b)
assert matrix_c_check == rotate_matrix_90(matrix_c)
assert matrix_d_check == rotate_matrix_90(matrix_d)
assert matrix_e_check == rotate_matrix_90(matrix_e)
