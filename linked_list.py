#
# Created by pronind.com
#


class Node:

    def __init__(self, data=None, link=None):
        self.data = data
        self.link = link


class LinkedList:

    def __init__(self):
        self.length = 0
        self.head = None
        self.last = None

    def __add__(self, other):
        if isinstance(other, LinkedList):
            self.last = other.head
            self.last = other.last
            self.length += other.length
            return self.length + other.length

    def add(self, new_node):
        if not self.head:
            self.head = new_node
            self.last = new_node
            self.head.link = None
            self.length = 1
        else:
            self.last.link = new_node
            self.last = new_node
            self.length += 1

    def __iter__(self):
        node = self.head
        if node == self.head:
            yield node
        while node != self.last:
            node = node.link
            yield node

n = Node(1)
n1 = Node(2)
n2 = Node(3)
n3 = Node(4)
n4 = Node(5)

ll = LinkedList()
lk = LinkedList()

ll.add(n)
ll.add(n1)
ll.add(n2)

for i in ll:
    print(i.data)

lk.add(n1)
lk.add(n2)
lk.add(n3)
lk.add(n4)

ll + lk

print('ll length', ll.length)

for i in ll:
    print('ll now is', i.data)

for i in lk:
    print('lk now is', i.data)
