#
# Created by pronind.com
#

from threading import Thread
import urllib.request
import time

test_url = "http://echo.msk.ru"


def get_url(url):
    data = urllib.request.urlopen(url)
    print(len(data.read()))


def with_thread():
    begin = time.time()
    for i in range(10):
        t = Thread(target=get_url, args=(test_url,))  # Comma has to be there ...
        t.start()
        t.join()
    print(time.time() - begin)


def without_thread():
    begin = time.time()
    for i in range(10):
        get_url(test_url)
    print(time.time() - begin)

with_thread()
without_thread()
