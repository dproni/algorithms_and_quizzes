#
# Created by pronind.com
#

import itertools
import timeit

list_of_values = ['A', 'B', 'C']
N = 3


def convert_to_custom_base(value, base=10):
    if value == 0:  # DO NOT FORGET
        return str(0)  # should return string
    binary = []  # array or string
    while value > 0:
        binary.append(str(value % base))  # % - what is left after dividing a / b. 3 / 2 == 1
        value //= base
    return ''.join(binary)[::-1]


def lib_permutations(itera, N):
    m = []
    for i in range(N):
        m.append(itera)
    result = []
    for i in itertools.product(*m):
        result.append(list(i))
    return result


def stupid_permutations(itera, N):  # N = 3
    vars_to_summ = ""
    result = []
    code_to_execute = ""
    for i in range(N):
        code_to_execute += "for k{0} in itera:".format(i)
        code_to_execute += "\n" + "\t" * (i + 2)
        vars_to_summ += "k" + str(i)
        vars_to_summ += ", "
    code_to_execute += "result.append([{0}])".format(vars_to_summ[:-2])
    exec(code_to_execute)
    return result


def smart_permutations(itera, N):
    i = 0
    result = []
    while True:
        short_result = []
        for k in map(lambda x: itera[int(x)], convert_to_custom_base(i, len(itera)).rjust(N, '0')):  # voodoo magic
            short_result.append(k)
        i += 1
        result.append(short_result)
        if i == len(itera) ** N:
            return result


t = timeit.Timer(lambda: smart_permutations(list_of_values, 10))
print(t.timeit(1))
t = timeit.Timer(lambda: lib_permutations(list_of_values, 10))
print(t.timeit(1))
t = timeit.Timer(lambda: stupid_permutations(list_of_values, 10))
print(t.timeit(1))




