#
# Created by pronind.com
#

from math import sqrt


def fibonacci_math(n):
    return int(((1 + sqrt(5)) ** n - (1 - sqrt(5)) ** n) / (2 ** n * sqrt(5)))


assert fibonacci_math(0) == 0
assert fibonacci_math(1) == 1
assert fibonacci_math(2) == 1
assert fibonacci_math(3) == 2
assert fibonacci_math(4) == 3
assert fibonacci_math(10) == 55


def fibonacci_sad(i):
    try:
        int(i)
        if i == 0:
            return [i]
        else:
            array = []
            for k in range(i):
                if not array:
                    array.append(k + 1)
                if len(array) == 1:
                    array.append(k + 1)
                else:
                    array.append(array[-1] + array[-2])
            return array
    except ValueError:
        return "wrong type"

assert fibonacci_sad(0) == [0]
assert fibonacci_sad(1) == [1, 1]
assert fibonacci_sad(2) == [1, 1, 2]
assert fibonacci_sad(3) == [1, 1, 2, 3]
assert fibonacci_sad(4) == [1, 1, 2, 3, 5]
assert fibonacci_sad(10) == [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]


def fibonacci_recursive(n):
    if n == 1:
        return 1
    elif n == 0:
        return 0
    else:
        return fibonacci_recursive(n-1) + fibonacci_recursive(n-2)


assert fibonacci_recursive(0) == 0
assert fibonacci_recursive(1) == 1
assert fibonacci_recursive(2) == 1
assert fibonacci_recursive(3) == 2
assert fibonacci_recursive(4) == 3
assert fibonacci_recursive(10) == 55

