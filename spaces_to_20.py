#
# Created by pronind.com
#

normal_string = "But I need more than myself this time."
normal_string_check = "But%20I%20need%20more%20than%20myself%20this%20time."

many_spaces = "Red hot chili peppers,  red  hot      peppers"
many_spaces_check = "Red%20hot%20chili%20peppers,%20red%20hot%20peppers"

strange_symbols = "!@#$%^&*(_)(*&^%$#@%@)@#$%^&*()(*&^%$#%20SEX OMG"
strange_symbols_check = "!@#$%^&*(_)(*&^%$#@%@)@#$%^&*()(*&^%$#%20SEX%20OMG"


def change_spaces_to_20(string):
    new_string = ""
    for i in range(len(string)):
        if string[i] == " ":
            if new_string[-3:] != "%20":
                new_string += "%20"
        else:
            new_string += string[i]
    print(new_string)
    return new_string


assert change_spaces_to_20(many_spaces) == many_spaces_check
assert change_spaces_to_20(normal_string) == normal_string_check
assert change_spaces_to_20(strange_symbols) == strange_symbols_check

