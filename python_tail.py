#
# Created by pronind.com
#

import os
import time
import argparse
import sys


class Tail:

    def __init__(self, file, lines=10, update_time=1):
        self.file_path = file
        self.file = None
        self.lines = -lines
        self.last_modified = None
        self.last_line = None
        self.update_time = update_time
        print("Tailing file: %s" % self.file_path)

    def is_modified(self):
        if not self.last_modified:
            self.last_modified = os.stat(self.file_path).st_mtime
            return True
        elif self.last_modified != os.stat(self.file_path).st_mtime:
            self.last_modified = os.stat(self.file_path).st_mtime
            return True
        else:
            return False

    def read_file(self):
        while True:
            if self.is_modified():
                self.file = open(self.file_path)
                lines = self.file.readlines()
                if not self.last_line:
                    self.last_line = len(lines)
                    for i in [line[:-1] for line in lines][self.lines:]:
                        print(i)
                else:
                    for i in [line[:-1] for line in lines][self.last_line:]:
                        print(i)
                self.file.close()
            else:
                time.sleep(self.update_time)

    def __exit__(self):
        self.file.close()


if __name__ == "__main__":
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument("file", help="File to tail")
        parser.add_argument("-n", help="Number of lines", type=int, default=10)
        parser.add_argument("-t", help="Update time", type=int, default=1)
        args = parser.parse_args()
        tail = Tail(args.file, args.n, args.t)
        tail.read_file()
    except KeyboardInterrupt:
        sys.exit(0)

