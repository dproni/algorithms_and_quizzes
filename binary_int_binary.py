#
# Created by pronind.com
#

values_to_test = [0, 1, 12, 100, 144453, 2311211212, -253]  # values even more than 8bit, just to ckech
values_to_compare = ['0', '1', '1100', '1100100', '100011010001000101', '10001001110000100100100011001100', '11111101']


def int_to_binary(i):
    if i == 0:  # DO NOT FORGET
        return str(0)  # should return string
    if i < 0:
        i *= -1  # remove negative sign
    binary = []  # array or string
    while i > 0:
        binary.append(str(i % 2))  # % - what is left after dividing a / b. 3 / 2 == 1
        i //= 2
    return ''.join(binary)[::-1]  # build string from array and reverse it


def convert_to_custom_base(value, base=10):
    if value == 0:  # DO NOT FORGET
        return str(0)  # should return string
    binary = []  # array or string
    while value > 0:
        binary.append(str(value % base))  # % - what is left after dividing a / b. 3 / 2 == 1
        value //= base
    return ''.join(binary)[::-1]


for k in range(len(values_to_test)):
    assert int_to_binary(values_to_test[k]) == values_to_compare[k]
