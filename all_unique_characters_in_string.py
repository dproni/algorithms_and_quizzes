#
# Created by pronind.com
#

string_a = "Mezmerize the simple minded Propaganda leaves us blinded"
string_b = "QWERTYUIOPASDFGHJKLZXCVBNM"
string_c = "ZAWSXEsasadfzxcaw"
string_d = "123123543"
string_e = "gtg"

string_a_check = False
string_b_check = True
string_c_check = False
string_d_check = False
string_e_check = False


def is_all_unique_no_additional_data_structure(string):
    print(string)
    for i in range(len(string)):
        same = 0
        for k in range(len(string)):
            if string[i] == string[k] and same < 2:
                same += 1
            if same == 2:
                return False
    return True


def is_all_unique(string):
    string_length = len(string)
    string_length_unique = len(set([i for i in string]))
    if string_length == string_length_unique:
        return True
    else:
        return False

assert is_all_unique(string_a) == string_a_check
assert is_all_unique(string_b) == string_b_check
assert is_all_unique(string_c) == string_c_check
assert is_all_unique(string_d) == string_d_check
assert is_all_unique(string_e) == string_e_check

assert is_all_unique_no_additional_data_structure(string_a) == string_a_check
assert is_all_unique_no_additional_data_structure(string_b) == string_b_check
assert is_all_unique_no_additional_data_structure(string_c) == string_c_check
assert is_all_unique_no_additional_data_structure(string_d) == string_d_check
assert is_all_unique_no_additional_data_structure(string_e) == string_e_check
