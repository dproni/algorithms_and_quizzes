#
# Created by pronind.com
#


class Node:

    def __init__(self, data, left=None, right=None):
        self.left = left
        self.data = data
        self.right = right

    def __str__(self):
        return str(self.data)

    def __eq__(self, other):
        if self.data == other.data:
            return True
        else:
            return False


class Tree:

    def __init__(self):
        self.length = 0
        self.head = Node(None)

    def add(self, new_node):
        if self.head.data is None:
            self.head = new_node
            return self
        self._add(self.head, new_node)

    def _add(self, upper_node, new_node):
        if upper_node.data >= new_node.data:
            if not upper_node.left:
                upper_node.left = new_node
            else:
                self._add(upper_node.left, new_node)
        if upper_node.data < new_node.data:
            if not upper_node.right:
                upper_node.right = new_node
            else:
                self._add(upper_node.right, new_node)

    def find(self, value):
        if self.head.data == value:
            return self.head
        return self._find(self.head, value)

    def _find(self, node, value):
        if node.data == value:
            return node
        if node.left is None and node.right is None:
            return "{0} not found".format(value)
        if node.data > value:
            return self._find(node.left, value)
        if node.data < value:
            return self._find(node.right, value)

    def __str__(self):
        array_to_print = []
        for i in self._to_str(self.head):
            array_to_print.append(i)
        return str(array_to_print)

    def _to_str(self, node):
        if node.left:
            for i in self._to_str(node.left):
                yield i
        yield node.data
        if node.right:
            for i in self._to_str(node.right):
                yield i


node = Node(10)
node1 = Node(2)
node2 = Node(21)
node3 = Node(32)
node4 = Node(12)
tree = Tree()
tree.add(node)
tree.add(node1)
tree.add(node2)
tree.add(node3)
tree.add(node4)

assert str(tree.head) == str(10)
assert str(tree.head.left) == str(2)
assert not tree.head.left.left
assert str(tree.head.right) == str(21)
assert str(tree.head.right.right) == str(32)
assert tree.find(12) == Node(12)
assert tree.find(211) == "211 not found"
