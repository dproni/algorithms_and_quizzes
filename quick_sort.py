#
# Created by pronind.com
#


a = [14, 1, 3, 4, 2, 6, 8, 2, 10, 0]
assert_a = [0, 1, 2, 2, 3, 4, 6, 8, 10, 14]

count = 0


def quick_sort(arr):
    global count
    less = []
    equal = []
    greater = []

    if len(arr) > 1:
        pivot = arr[0]
        for x in arr:
            count += 1
            if x < pivot:
                less.append(x)
            if x == pivot:
                equal.append(x)
            if x > pivot:
                greater.append(x)
        return quick_sort(less) + equal + quick_sort(greater)
    else:
        return arr


print("length ok", len(quick_sort(a)) == len(assert_a))
print("order ok", quick_sort(a) == assert_a)
print(count)
