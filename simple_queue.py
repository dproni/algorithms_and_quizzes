#
# Created by pronind.com
#


class Node:

    def __init__(self, data):
        self.data = data
        self.next = None


class SimpleQueue:

    def __init__(self):
        self.length = 0
        self.head = None

    def queue(self, data):
        node = Node(data)
        self.length += 1
        n = self.head
        if not n:
            self.head = node
        else:
            while n:
                if n.next:
                    n = n.next
                else:
                    n.next = node
                    break

    def pop(self):
        if self.head:
            if self.head.next:
                self.length -= 1
                to_pop = self.head
                self.head = self.head.next
                return to_pop.data
            else:
                self.length -= 1
                to_pop = self.head.data
                self.head = None
                return to_pop
        else:
            return None

queue = SimpleQueue()
queue.queue('1')
queue.queue(2)
queue.queue('asdasd')
queue.queue('O')
queue.queue('M')
queue.queue('G')
queue.queue('is not GMO')

assert queue.pop() == '1'
assert queue.length == 6
assert queue.pop() == 2
assert queue.length == 5
assert queue.pop() == 'asdasd'
assert queue.length == 4
assert queue.pop() == 'O'
assert queue.length == 3
assert queue.pop() == 'M'
assert queue.length == 2
assert queue.pop() == 'G'
assert queue.length == 1
assert queue.pop() == 'is not GMO'
assert queue.length == 0
assert not queue.pop()
assert queue.length == 0
