#
# Created by pronind.com
#
# Algorithm to bypass 2d matrix using spiral path
#

a = [
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 10, 11, 12],
    [13, 14, 15, 16]
]

b = [
    [1,2,3],
    [4,5,6],
    [7,8,9]
]

c = [
    [1,2,3,4,5,6],
    [7,8,9,10,11,12],
    [13,14,15,16,17,18],
]

d = [
    [1, 2],
    [3, 4],
    [5, 6],
    [7, 8],
    [9, 10]
]

list_to_check_a = [1, 2, 3, 4, 8, 12, 16, 15, 14, 13, 9, 5, 6, 7, 11, 10]
list_to_check_b = [1, 2, 3, 6, 9, 8, 7, 4, 5]
list_to_check_c = [1, 2, 3, 4, 5, 6, 12, 18, 17, 16, 15, 14, 13, 7, 8, 9, 10, 11]
list_to_check_d = [1, 2, 4, 6, 8, 10, 9, 7, 5, 3]


def spiral(lst):
    arr = []  # new array
    x = y = 0  # cursor x and y coordinates
    x_max = len(lst[0]) - 1  # mamximum x position
    y_max = len(lst) - 1  # maximum y position
    tier = 0  # count how many times we are passing through array
    len_old_arr = len(lst) * len(lst[0])  # needs to stop iteration
    while True:  # why not?
        if x + tier < x_max and y == 0 :  # top row
            arr.append(lst[y + tier][x + tier])
            x += 1
            if len(arr) == len_old_arr:
                break
        if x + tier + tier == x_max and y < y_max:  # right column
            arr.append(lst[y + tier][x + tier])
            y += 1
            if len(arr) == len_old_arr:
                break
        if x > 0 and y == y_max - tier:  # bottom row
            arr.append(lst[y][x])
            x -= 1
            if len(arr) == len_old_arr:
                break
        if x == 0 and y > 0:  # left column
            arr.append(lst[y][x])
            y -= 1
            if len(arr) == len_old_arr:
                break
        if len(arr) and x == y == 0:  # iterate tier
            tier += 1
    print(arr)
    return arr

assert spiral(a) == list_to_check_a
assert spiral(b) == list_to_check_b
assert spiral(c) == list_to_check_c
assert spiral(d) == list_to_check_d

